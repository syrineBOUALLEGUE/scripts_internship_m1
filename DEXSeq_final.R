DEXSeq_final.R 

DEXSeq.R
patern= "^[mnat_*.txt]"
patern
countFiles = list.files("/Users/macbookair/gtf_File/mnat_castes/" , pattern= patern, full.names=TRUE)
head(countFiles)
countFiles
flattenedFile = list.files("/Users/macbookair/gtf_File/", pattern="mnat_dexseq.gff", full.names=TRUE)
flattenedFile
#[1] "/Users/macbookair/gtf_File//mnat_dexseq.gff" 

#Table creation : 
sampleTable = data.frame( row.names = c( "mnat_king_133", "mnat_king_135", "mnat_king_162", "mnat_queen_134", "mnat_queen_135", "mnat_queen_137", "mnat_soldierMajor_135", 
	"mnat_soldierMaMinor_137", "mnat_soldierMinor_135", "mnat_workersMajor_132", "mnat_workersMajor_133", "mnat_workersMajor_140", "mnat_workersMinor_135","mnat_workersMinor_137", "mnat_workersMinor_140" ),
 Caste = c("king", "king", "king", "queen", "queen", "queen", "soldierMajor", "soldierMaMinor", "soldierMinor", "workersMajor", "workersMajor", "workersMajor", "workersMinor" ," workersMinor", "workersMinor"
 ))
sampleTable
#to add extra column to the sampleTable 
sampleTable$king <-(ifelse(sampleTable$Caste=="king",1,0))
sampleTable$queen <-(ifelse(sampleTable$Caste=="queen",1,0))
sampleTable$soldier <-(ifelse(sampleTable$Caste=="soldierMinor"|sampleTable$Caste=="soldierMaMinor"|sampleTable$Caste=="soldierMajor",1,0))
sampleTable$workers <-(ifelse(sampleTable$Caste=="workersMajor"|sampleTable$Caste=="workersMinor",1,0))
sampleTable$workers[14] <- 1

# Testing for differential exon usage
library( "DEXSeq" )

################################# For king #################################
dxd_king= DEXSeqDataSetFromHTSeq(
   countFiles,
   sampleData=sampleTable,
   design= ~ sample + exon + king:exon,   #to mesure the expression for the king
   flattenedfile=flattenedFile )

dxd_king = estimateSizeFactors( dxd_king )
dxd_king= estimateDispersions( dxd_king )
dxd_king = testForDEU( dxd_king) # test for differntial exon usage 
dxd_king = estimateExonFoldChanges( dxd_king, fitExpToVar="king") # gives the pvalue 
dxr_king= DEXSeqResults( dxd_king)
save(dxr_king, file = "dxr_king.RData") # to have the original format of the file 
dim (dxr_king)
#[1] 79769    13

mcols(dxr_king)$description
table ( dxr_king$padj < 0.05 ) # expression differentielle des exons
#FALSE  TRUE 
#66290     1 
table ( tapply( dxr_king$padj < 0.05, dxr_king$groupID, any ) ) # donne information sur les gènes exprimés (gène concernés pa ces exons ) 
#FALSE  TRUE 
# 7157     1 
plotMA( dxr_king, cex=0.8 )

dxr_king_df <- data.frame(dxr_king)
dim(dxr_king_df) 
#[1] 79769    31
dxr_king_test = dxr_king_df[dxr_king_df$padj < 0.05, ]
 dim(dxr_king_test)
#[1] 13479    31

write.table (dxr_king_df, file = "dxr_king_df.txt" , sep= "\t" , col.names= TRUE )  # chemin : [1] "/Users/macbookair"
write.table (dxr_king_test, file = "dxr_king_test.txt" , sep= "\t" , header= TRUE, col.names= TRUE ) 



############################################## Modification Anna ##############################################

dxr_king_df <- data.frame(dxr_king) #### Donc ici tu crees ta dataframe
dim(dxr_king_df)  #### tu attrape ici le nombre total de colonnes
#[1] 79769    31

dxr_king_df_withoutNA <- na.omit(dxr_king_df) #### tu enleves les NA 
dim(dxr_king_df_withoutNA)
[#1] 66291    31
dxr_king_df_Final <- dxr_king_df_withoutNA[dxr_king_df_withoutNA$padj <0.05, ] #### tu crees une dataframe avec uniquement les p-value en dessous de 0.05
dim(dxr_king_df_Final)
#[1] 2297   31   ||  1 31 
table (dxr_king_df_Final$padj <0.05) #### le nombre d exons differentiellement exprimeés = 2297
#TRUE 
#2297  || 1 
table ( tapply(dxr_king_df_Final$padj < 0.05, dxr_king_df_Final$groupID, any ) ) #### le nombre de genes ayant des exons differentiellement exprimes =1576 
#TRUE 
#1576  || 1
listeDoublons <- dxr_king_df_Final$groupID ## ici tu recuperes la liste des genes qui ont un exon surrexprimes
ListeGenesKing <- unique(listeDoublons) ## ici tu enleves les doublons et recuperes donc les bons genes


write.table (ListeGenesKing, file = "listeGenesKing.txt" , sep= "\t" , col.names= TRUE )  # ici tu enregistre la liste des genes

write.table (dxr_king_df, file = "dxr_king_df.txt" , sep= "\t" , col.names= TRUE )  # chemin : [1] "/Users/macbookair"

########################################################################################################################
Liste_Exon_King <- dxr_king_df_Final$exonBaseMean   ## à vérifier (la légende d'exon)
Liste_Exon_King
#[1] 188.528
write.table(Liste_Exon_King, file ="Exon_king", sep = "\t", col.names = TRUE)

########################################################################################################################




################################# For queen #################################
dxd_queen= DEXSeqDataSetFromHTSeq(
   countFiles,
   sampleData=sampleTable,
   design= ~ sample + exon + queen:exon,   
   flattenedfile=flattenedFile )

dxd_queen = estimateSizeFactors( dxd_queen )
dxd_queen= estimateDispersions( dxd_queen )
dxd_queen = testForDEU( dxd_queen)# test for differntial exon usage 
dxd_queen = estimateExonFoldChanges( dxd_queen, fitExpToVar="queen") # donne les pvalue 
dxr_queen = DEXSeqResults( dxd_queen)
dim(dxr_queen)
#[1] 79769     13

mcols(dxr_queen)$description
table ( dxr_queen$padj < 0.05 ) # expression differentielle des exons
#FALSE  TRUE 
#41051 11256 
table ( tapply( dxr_queen$padj < 0.05, dxr_queen$groupID, any ) ) # donne information sur les gènes exprimés (gène concernés pa ces exons ) 
#FALSE  TRUE 
# 2434  3547 
plotMA( dxr_queen, cex=0.8 )

dxr_queen_df <- data.frame(dxr_queen)
dim(dxr_queen_df)
#[1] 79769    31
dxr_queen_test = dxr_queen_df[dxr_queen_df$padj < 0.05, ]
dim(dxr_queen_test)
#[1] 38718    31
write.table (dxr_queen_df, file = "dxr_queen_df.txt" , sep= "\t" , col.names= TRUE )  
write.table (dxr_queen_test, file = "dxr_queen_test.txt" , sep= "\t" , col.names= TRUE ) 


############################################## Modification Anna ##############################################

dxr_queen_df <- data.frame(dxr_queen) #### Donc ici tu crees ta dataframe
dim(dxr_queen_df)  #### tu attrape ici le nombre total de colonnes
#[1] 79769    13

dxr_queen_df_withoutNA <- na.omit(dxr_queen_df) #### tu enleves les NA 
dim(dxr_queen_df_withoutNA)
#[1] 52307    31
dxr_queen_df_Final <- dxr_queen_df_withoutNA[dxr_queen_df_withoutNA$padj <0.05, ] #### tu crees une dataframe avec uniquement les p-value en dessous de 0.05
dim(dxr_queen_df_Final)
# 11256    31
table (dxr_queen_df_Final$padj <0.05) #### le nombre d exons differentiellement exprimeés = 16848
#TRUE 
# 11256 
table ( tapply(dxr_queen_df_Final$padj < 0.05, dxr_queen_df_Final$groupID, any ) ) #### le nombre de genes ayant des exons differentiellement exprimes =1576 
#TRUE 
# 3547 
listeDoublons_queen <- dxr_queen_df_Final$groupID ## ici tu recuperes la liste des genes qui ont un exon surrexprimes
ListeGenesQueen <- unique(listeDoublons_queen) ## ici tu enleves les doublons et recuperes donc les bons genes


write.table (ListeGenesQueen, file = "listeGenesQueen.txt" , sep= "\t" , col.names= TRUE )  # ici tu enregistre la liste des genes

write.table (dxr_queen_df, file = "dxr_queen_df.txt" , sep= "\t" , col.names= TRUE )  # chemin : [1] "/Users/macbookair"

########################################################################################################################
Liste_Exon_Queen <- dxr_queen_df_Final$exonBaseMean   ## à vérifier (la légende d'exon)
Liste_Exon_Queen
#[1] 188.528
write.table(Liste_Exon_Queen, file ="Exon_Queen", sep = "\t", col.names = TRUE)

########################################################################################################################





################################# For workers #################################

dxd_workers = DEXSeqDataSetFromHTSeq(
   countFiles,
   sampleData=sampleTable,
   design= ~ sample + exon + workers:exon, 
   flattenedfile=flattenedFile )

dxd_workers = estimateSizeFactors( dxd_workers )
dxd_workers = estimateDispersions( dxd_workers )
dxd_workers = testForDEU( dxd_workers)# test for differntial exon usage 
dxd_workers = estimateExonFoldChanges( dxd_workers, fitExpToVar="workers")
dxr_workers= DEXSeqResults( dxd_workers)
dim(dxr_workers)
#[1] 79769    13

table ( dxr_workers$padj < 0.05 ) # expression differentielle des exons
#FALSE  TRUE 
#66211    80 
table ( tapply( dxr_workers$padj < 0.05, dxr_workers$groupID, any ) ) # donne information sur les gènes exprimés (gène concernés pa ces exons ) 
#FALSE  TRUE 
#7118    67 
plotMA( dxr_workers, cex=0.8 )

dxr_workers_df <- data.frame(dxr_workers)
dim(dxr_workers_df)
#[1] 79769    31

dxr_workers_test = dxr_workers_df[dxr_workers_df$padj < 0.05, ]
dim(dxr_workers_test)
#[1] 13558    31

write.table (dxr_workers_df, file = "dxr_workers_df.txt" , sep= "\t" , col.names= TRUE )  
write.table (dxr_workers_test, file = "dxr_workers_test.txt" , sep= "\t", col.names= TRUE )


############################################## Modification Anna ##############################################

dxr_workers_df <- data.frame(dxr_workers) #### Donc ici tu crees ta dataframe
dim(dxr_workers_df)  #### tu attrape ici le nombre total de colonnes
#[1] 79769    31

dxr_workers_df_withoutNA <- na.omit(dxr_workers_df) #### tu enleves les NA 
dim(dxr_workers_df_withoutNA)
#[1] 66291    31
dxr_workers_df_Final <- dxr_workers_df_withoutNA[dxr_workers_df_withoutNA$padj <0.05, ] #### tu crees une dataframe avec uniquement les p-value en dessous de 0.05
dim(dxr_workers_df_Final)
#[1] 4097   31 ||   80  31
table (dxr_workers_df_Final$padj <0.05) #### le nombre d exons differentiellement exprimeés = 2297
#TRUE || TRUE 
#4097 ||   80
table ( tapply(dxr_workers_df_Final$padj < 0.05, dxr_workers_df_Final$groupID, any ) ) #### le nombre de genes ayant des exons differentiellement exprimes =1576 
#TRUE || TRUE 
#2429 ||  67 
listeDoublons_workers <- dxr_workers_df_Final$groupID ## ici tu recuperes la liste des genes qui ont un exon surrexprimes
ListeGenesWorkers <- unique(listeDoublons_workers) ## ici tu enleves les doublons et recuperes donc les bons genes


write.table (ListeGenesWorkers, file = "listeGenesworkers.txt" , sep= "\t" , col.names= TRUE )  # ici tu enregistre la liste des genes

write.table (dxr_workers_df, file = "dxr_workers_df.txt" , sep= "\t" , col.names= TRUE )  # chemin : [1] "/Users/macbookair"


########################################################################################################################
Liste_Exon_workers <- dxr_workers_df_Final$exonBaseMean   ## à vérifier (la légende d'exon)
Liste_Exon_workers
#[1] 188.528
write.table(Liste_Exon_workers, file ="Exon_workers", sep = "\t", col.names = TRUE)

########################################################################################################################





################################# For For Soldiers #################################

dxd_soldiers = DEXSeqDataSetFromHTSeq(
   countFiles,
   sampleData=sampleTable,
   design= ~ sample + exon + soldier:exon, 
   flattenedfile=flattenedFile )

dxd_soldiers = estimateSizeFactors( dxd_soldiers )
dxd_soldiers = estimateDispersions( dxd_soldiers )
dxd_soldiers = testForDEU( dxd_soldiers)
dxd_soldiers = estimateExonFoldChanges( dxd_soldiers, fitExpToVar= "soldier")
dxr_soldiers= DEXSeqResults( dxd_soldiers)
dim(dxr_soldiers)
#[1] 79769    13

table ( dxr_soldiers$padj < 0.05 ) # expression differentielle des exons
#FALSE  TRUE 
#63252    53 
table ( tapply( dxr_soldiers$padj < 0.05, dxr_soldiers$groupID, any ) ) # donne information sur les gènes exprimés (gène concernés pa ces exons ) 
#FALSE  TRUE 
#6386    26 
plotMA( dxr_soldiers, cex=0.8 )

dxr_soldiers_df <- data.frame(dxr_soldiers)
dim(dxr_soldiers_df)
#[1] 79769    31
dxr_soldiers_test = dxr_soldiers_df[dxr_soldiers_df$padj < 0.05, ]
dim(dxr_soldiers_test)
#[1] 16517    31
write.table (dxr_soldiers_df, file = "dxr_soldiers_df.txt" , sep= "\t" , col.names= TRUE )  
write.table (dxr_soldiers_test, file = "dxr_soldiers_test.txt" , sep= "\t", col.names= TRUE )

############################################## Modification Anna ##############################################

dxr_soldiers_df <- data.frame(dxr_soldiers) #### Donc ici tu crees ta dataframe
dim(dxr_soldiers_df)  #### tu attrape ici le nombre total de colonnes
#[1] 79769    31

dxr_soldiers_df_withoutNA <- na.omit(dxr_soldiers_df) #### tu enleves les NA 
dim(dxr_soldiers_df_withoutNA)
#[1] 63305    31
dxr_soldiers_df_Final <- dxr_soldiers_df_withoutNA[dxr_soldiers_df_withoutNA$padj <0.05, ] #### tu crees une dataframe avec uniquement les p-value en dessous de 0.05
dim(dxr_soldiers_df_Final)
#[1] 2407   31 ||  53 31
table (dxr_soldiers_df_Final$padj <0.05) #### le nombre d exons differentiellement exprimeés = 2407 
#TRUE || FALSE  TRUE 
#2407 || 2354    53 
table ( tapply(dxr_soldiers_df_Final$padj < 0.05, dxr_soldiers_df_Final$groupID, any ) ) #### le nombre de genes ayant des exons differentiellement exprimes =1576 
#TRUE   || FALSE  TRUE 
#1576   ||  1538    26
listeDoublons_soldiers <- dxr_soldiers_df_Final$groupID ## ici tu recuperes la liste des genes qui ont un exon surrexprimes
ListeGenesSoldiers <- unique(listeDoublons_soldiers) ## ici tu enleves les doublons et recuperes donc les bons genes


write.table (ListeGenesSoldiers, file = "listeGenesSoldiers.txt" , sep= "\t" , col.names= TRUE )  # ici tu enregistre la liste des genes

write.table (dxr_soldiers_df, file = "dxr_soldiers_df.txt" , sep= "\t" , col.names= TRUE )  # chemin : [1] "/Users/macbookair"

########################################################################################################################
Liste_Exon_soldiers <- dxr_soldiers_df_Final$exonBaseMean   ## à vérifier (la légende d'exon)
Liste_Exon_soldiers <- dxr_soldiers_df_Final$featureID
Liste_Exon_soldiers <- dxr_soldiers_df_Final$groupID
Liste_Exon_soldiers
#[1] 188.528
write.table(Liste_Exon_soldiers, file ="Exon_soldiers", sep = "\t", col.names = TRUE)

########################################################################################################################




##### représentation Graphique #####
if (!(suppressMessages(require(psych)))){install.packages("psych")}
library(VennDiagram)
# Préparation of data : 
GDE <- read.table("GDE.txt", header=TRUE, sep = "\t")
 GDE$GDE[GDE$Sample == "King"]
#[1] 1576

ListeGenesKing <- read.table("listes_Genes_King.txt")
ListeGenesQueen <- read.table("Liste_Genes_Queen.txt")
ListeGenesSoldiers <- read.table("Liste_Gene_Soldier.txt")
ListeGenesWorkers <- read.table("liste_Genes_Workers.txt")

Liste_queen_soldier <- read.table ("Reine_soldier.txt")
Liste_queen_worker <- read.table ("Reine_worker.txt")
Liste_queen_worker_soldier <- read.table ("Reine_worker_soldier.txt")
Liste_King_queen <- read.table ("Roi_Reine.txt")
Liste_King_queen_soldier <- read.table ("Roi_reine_soldier.txt")
Liste_King_queen_worker <- read.table ("Roi_reine_worker.txt")
Liste_King_soldier <- read.table ("Roi_soldier.txt")
Liste_King_worker<- read.table ("Roi_worker.txt")

venn.diagram(
x = list( Liste_queen_soldier , Liste_queen_worker, Liste_queen_worker_soldier ,Liste_King_queen ),

filename = "Venn_diagram_Gene_differentially_expressed.png",
    main="Venn diagram Gene differentially expressed",
        output = TRUE ,
        imagetype="png" ,
        height = 700 , 
        width =750, 
        resolution = 200,
        compression = "lzw",
        lwd = 1,
        lty = 'blank',
    category = c("King", "Queen", "Soldier", "Workers" ),
           fill = c('#4DAC26','#5E3C99', '#018571', '#E66101'),
        cex = 1,
        fontfamily = "sans",
           cat.pos=5,
        cat.default.pos = "outer",
    cat.cex=1,
       cat.fontface = "bold"
        )

plotDEXSeq( dxr_king_df_Final, "Mnat_00244", displayTranscripts=TRUE, legend=TRUE, cex.axis=1.2, cex=1.3, lwd=2 )


### Generation of Venn Diagramm ####
King <- read.table("listes_Genes_king.txt",header=F,quote="")
Queen<- read.table("Liste_Genes_Queen.txt",header=F,quote="")
Soldier<-read.table("Liste_Gene_Soldier.txt",header=F,quote="")
Workers<-read.table("liste_Genes_Workers.txt",header=F,quote="")
venn.diagram(
x = list(King$V1 ,Queen$V1,Soldier$V1,Workers$V1),
filename = "Venn_diagram_Gene_differencially_expressed_Commun_caste.png",
    main="Venn diagram Gene differencially Expressed ",
        output = TRUE ,
        imagetype="png" ,
        height = 700 , 
        width =750, 
        resolution = 200,
        compression = "lzw",
        lwd = 1,
        lty = 'blank',
    category = c("King", "Queen", "Soldiers", "Workers" ),
        fill = c('#4DAC26','#5E3C99', '#018571', '#E66101'),
        cex = 1,
        fontfamily = "sans",
           cat.pos=5,
        cat.default.pos = "outer",
    cat.cex=1,
       cat.fontface = "bold"
        )

########################################################################################################################
library(ggplot2)
library(gridExtra)
library(ggpubr)
library(tidyverse)
library(car)
library(dplyr)
library(Hmisc)
library(reshape2)
############################################ Domains ############################################
############################################ Domain graphics ############################################

# aprés avoir trier les données par ordre décroissant et séléctionner les 50 colonnes  : 
Domains <- read.table ("Domains.csv" , header = TRUE ,sep = ";")
head(Domains) # pour vérifier qu'on a bien importer le tableau 
library(reshape2)
Domains<-Domains[order(Domains$sum, decreasing= F),] # Permet de trier les valeur de plus haut au plus bas ( domaine ou il y a plus de gènes : qualtitatif )
Domains$Domain <- factor(Domains$Domain, levels=Domains$Domain[order(Domains$sum)]) # sert à fixer l'ordre des domaines pour ne pas bouger 
Domains <- Domains[,-6] # to not displau "sum"
# Sert à généerer un graphique qualitative pour les domains ( combien de gène par domaines) 
#Domains_melted <- melt (Domains) # change the represnetation from horizental to vertical 
Domains_melted <- melt(Domains,id.var="Domain")
Domains_melted

#Graphic representation: 

library(ggplot2)

Domains_graph<-ggboxplot(Domains_melted, x = "Domain", y = "value",
                color = "variable",fill="variable",
                add = "jitter", shape = 21,size=1,alpha=0.2)+
guides(color = FALSE)+
 stat_summary(fun.data=median_hilow,size=0.1)+
  coord_flip() +
  xlab("Domains")+
  ylab("Number of exons counted for each caste")+
  ggtitle("Representation of 48 domains for the exons expressed for each caste")+ 
theme(
    panel.grid.major.y = element_blank(),
    panel.border = element_blank(),
    axis.title.x = element_blank(),
    axis.line = element_line(colour = "black", size = 0.4, linetype = "solid"),
    axis.text.x=element_text(size=8),
    axis.text.y=element_text(size=6),
    legend.title = element_blank(),
    legend.text = element_text(size = 8),
    legend.position="right",
    legend.key.size = unit(2, "cm"),
    legend.key.width = unit(1,"cm"))

# Saving Graphics 
ggsave(Domains_graph, file= "Domains_graph.pdf", width=10, height=10)
ggsave(Domains_graph, file= "Domains_graph.png", width=10, height=8)


######################################################################################################
##king
plotDispEsts( dxd_king)
plotMA( dxr_king, cex=0.8 )
dxr_king <- write.table(dxr_king, "dxr_king.txt")

#sprintf => s pour scanner et printer 
king=sprintf("King")
Queen=sprintf("Queen")
Worker=sprintf("Worker")
Soldiers=sprintf("Soldiers")

dxr_king@modelFrameBM$Caste <-factor(dxr_king@modelFrameBM$Caste,levels=c( "workersMinor","king","queen","soldierMajor","soldierMaMinor","soldierMinor","workersMajor","workersMinor"))

##############################################################################################################################################################
sampleTable = data.frame( row.names = c( "mnat_king_133", "mnat_king_135", "mnat_king_162", "mnat_queen_134", "mnat_queen_135", "mnat_queen_137", "mnat_soldierMajor_135", 
  "mnat_soldierMaMinor_137", "mnat_soldierMinor_135", "mnat_workersMajor_132", "mnat_workersMajor_133", "mnat_workersMajor_140", "mnat_workersMinor_135","mnat_workersMinor_137", "mnat_workersMinor_140" ),
 Caste = c("king", "king", "king", "queen", "queen", "queen", "soldier", "soldier", "soldier", "worker", "worker", "worker", "worker" ," worker", "worker" ))
sampleTable
#to add extra column to the sampleTable 
sampleTable$king <-(ifelse(sampleTable$Caste=="king",1,0))
sampleTable$queen <-(ifelse(sampleTable$Caste=="queen",1,0))
sampleTable$soldier <-(ifelse(sampleTable$Caste=="soldier", 0,1))
sampleTable$workers <-(ifelse(sampleTable$Caste=="worker",1,0))
sampleTable$workers[14] <- 1

# Visualization of differential expression 

load("dxr_king.RData")
levels(dxr_king@modelFrameBM$Caste) # we can see that we have 5 levels ==> we should have only 4 : king , queen , worker , soldier
dxr_king@modelFrameBM$caste # Null
dxr_king@modelFrameBM$caste <- factor(dxr_king@modelFrameBM$caste, levels =c("king", "queen", "soldier","worker"))
dxr_king@modelFrameBM$Caste[2055:2212]<-"worker" 
dxr_king@modelFrameBM$Caste<-factor(dxr_king@modelFrameBM$Caste,levels=c("king","queen","soldier" ,"worker"))
save(dxr_king,file="dxr_global.RData")
load("dxr_global.RData")

###GRAPHIQUE
#Creation legend graphique
King=sprintf("King (n= %s)",length(grep("^mnat_king",levels(dxr_king@modelFrameBM$sample))))# les king on 3 échantillons 
Queen=sprintf("Queen (n= %s)",length(grep("^mnat_queen",levels(dxr_king@modelFrameBM$sample))))#Queen 3echantillons ("mnat_queen_134","mnat_queen_135","mnat_queen_137")
Soldiers=sprintf("Soldiers (n= %s)",length(grep("^mnat_soldier",levels(dxr_king@modelFrameBM$sample)))) #Soldiers  3 ech("mnat_soldierMajor_135","mnat_soldierMaMinor_137","mnat_soldierMinor_135")
Workers=sprintf("Workers (n= %s)",length(grep("^mnat_workers",levels(dxr_king@modelFrameBM$sample))))# workers 6 ech "mnat_workersMajor_132" "mnat_workersMajor_133" "mnat_workersMajor_140"  
# "mnat_workersMinor_135"   "mnat_workersMinor_137" "mnat_workersMinor_140"

pdf("Expression_Mnat_05970_Caste.pdf",width=55,height=24)
png ("Expression_Mnat_05970_Caste.png")
plotDEXSeq( dxr_king, "Mnat_05970",FDR=0.1, fitExpToVar="Caste",expression=TRUE, splicing=T,displayTranscripts=T,
cex.axis=1.2, cex=1.8, lwd=2,legend=FALSE)
par(xpd=TRUE) # for the legend (king , queen , soldiers and workers )
legend("topright",c(King,Queen,Soldiers,Workers),
      col=c("blue", "green","purple","orange"),lty="solid",lwd=2,cex=1.6,text.font=1)
dev.off() # to close the block 

##############################################################################################################################################################
################# for the mnat_00112

png ("Expression_Mnat_00112_Caste.png")
plotDEXSeq( dxr_king, "Mnat_00112",FDR=0.1, fitExpToVar="Caste",expression=TRUE, splicing=T,displayTranscripts=T,
cex.axis=1.2, cex=1.8, lwd=2,legend=FALSE)
par(xpd=TRUE) # for the legend (king , queen , soldiers and workers )
legend("topright",c(King,Queen,Soldiers,Workers),
      col=c("blue", "green","purple","orange"),lty="solid",lwd=2,cex=1.6,text.font=1)
dev.off()
 ################# for the Mnat_00189

png ("Expression_Mnat_00189_Caste.png")
plotDEXSeq( dxr_king, "Mnat_00189",FDR=0.1, fitExpToVar="Caste",expression=TRUE, splicing=T,displayTranscripts=T,
cex.axis=1.2, cex=1.8, lwd=2,legend=FALSE)
par(xpd=TRUE) # for the legend (king , queen , soldiers and workers )
legend("topright",c(King,Queen,Soldiers,Workers),
      col=c("blue", "green","purple","orange"),lty="solid",lwd=2,cex=1.6,text.font=1)
dev.off()

 ################# for the Mnat_00322

png ("Expression_Mnat_00322_Caste.png")
plotDEXSeq( dxr_king, "Mnat_00322",FDR=0.1, fitExpToVar="Caste",expression=TRUE, splicing=T,displayTranscripts=T,
cex.axis=1.2, cex=1.8, lwd=2,legend=FALSE)
par(xpd=TRUE) # for the legend (king , queen , soldiers and workers )
legend("topright",c(King,Queen,Soldiers,Workers),
      col=c("blue", "green","purple","orange"),lty="solid",lwd=2,cex=1.6,text.font=1)
dev.off()

 ################# for the Mnat_00382
png ("Expression_Mnat_00382_Caste.png")
plotDEXSeq( dxr_king, "Mnat_00382",FDR=0.1, fitExpToVar="Caste",expression=TRUE, splicing=T,displayTranscripts=T,
cex.axis=1.2, cex=1.8, lwd=2,legend=FALSE)
par(xpd=TRUE) # for the legend (king , queen , soldiers and workers )
legend("topright",c(King,Queen,Soldiers,Workers),
      col=c("blue", "green","purple","orange"),lty="solid",lwd=2,cex=1.6,text.font=1)
dev.off()

 ################# for the Mnat_00399
png ("Expression_Mnat_00399_Caste.png")
plotDEXSeq( dxr_king, "Mnat_00399",FDR=0.1, fitExpToVar="Caste",expression=TRUE, splicing=T,displayTranscripts=T,
cex.axis=1.2, cex=1.8, lwd=2,legend=FALSE)
par(xpd=TRUE) # for the legend (king , queen , soldiers and workers )
legend("topright",c(King,Queen,Soldiers,Workers),
      col=c("blue", "green","purple","orange"),lty="solid",lwd=2,cex=1.6,text.font=1)
dev.off()

 ################# for the Mnat_00644
png ("Expression_Mnat_00644_Caste.png")
plotDEXSeq( dxr_king, "Mnat_00644",FDR=0.1, fitExpToVar="Caste",expression=TRUE, splicing=T,displayTranscripts=T,
cex.axis=1.2, cex=1.8, lwd=2,legend=FALSE)
par(xpd=TRUE) # for the legend (king , queen , soldiers and workers )
legend("topright",c(King,Queen,Soldiers,Workers),
      col=c("blue", "green","purple","orange"),lty="solid",lwd=2,cex=1.6,text.font=1)
dev.off()

 ################# for the Mnat_10701
png ("Expression_Mnat_10701_Caste.png")
plotDEXSeq( dxr_king, "Mnat_10701",FDR=0.1, fitExpToVar="Caste",expression=TRUE, splicing=T,displayTranscripts=T,
cex.axis=1.2, cex=1.8, lwd=2,legend=FALSE)
par(xpd=TRUE) # for the legend (king , queen , soldiers and workers )
legend("topright",c(King,Queen,Soldiers,Workers),
      col=c("blue", "green","purple","orange"),lty="solid",lwd=2,cex=1.6,text.font=1)
dev.off()

 ################# for the Mnat_12778
png ("Expression_Mnat_12778_Caste.png")
plotDEXSeq( dxr_king, "Mnat_12778",FDR=0.1, fitExpToVar="Caste",expression=TRUE, splicing=T,displayTranscripts=T,
cex.axis=1.2, cex=1.8, lwd=2,legend=FALSE)
par(xpd=TRUE) # for the legend (king , queen , soldiers and workers )
legend("topright",c(King,Queen,Soldiers,Workers),
      col=c("blue", "green","purple","orange"),lty="solid",lwd=2,cex=1.6,text.font=1)
dev.off()
##############################################################################################################################################################

##Queen
plotDispEsts( dxd_queen)
plotMA( dxr_queen, cex=0.8 )

##Worker
plotDispEsts( dxd_workers)
plotMA( dxr_workers, cex=0.8 )

##Soldiers
plotDispEsts( dxd_soldiers)
plotMA( dxr_soldiers, cex=0.8 )


##############################################################################################################################################################
#### Venn diagramm for the commun domains 
domains<-read.table("domains.csv",header=T,sep=";")
dim(domains)
[1] 51  6

domains_communs_4_caste<-subset(domains, King !=0 & Queen!=0 & Soldier !=0 & Worker !=0)
dim(domains_communs_4_caste)

library(tibble)
library(VennDiagram)

King <- read.table("List_Dommains_Communs_king.txt",header=F,quote="")
Queen<- read.table("List_Dommains_Communs_Queen.txt",header=F,quote="")
Soldier<-read.table("List_Dommains_Communs_Soldier.txt",header=F,quote="")
Workers<-read.table("List_Dommains_Communs_Worker.txt",header=F,quote="")


#venn.diagram(
x = list(King$V1 ,Queen$V1,Soldier$V1,Workers$V1),
filename = "Venn_diagram_interaction_domains.png",
    main="Venn diagram of commun domains between the castes ",
        output = TRUE ,
        imagetype="png" ,
        height = 700 , 
        width =750, 
        resolution = 200,
        compression = "lzw",
        lwd = 1,
        lty = 'blank',
    category = c("King", "Queen", "Soldier", "Worker" ),
        fill = c('#4DAC26','#5E3C99', '#018571', '#E66101'),
        cex = 1,
        fontfamily = "sans",
           cat.pos=5,
        cat.default.pos = "outer",
    cat.cex=1,
       cat.fontface = "bold"
        )



##Doamins greater that 0 
#Library
library(tibble)
library(VennDiagram)
##################

King <- read.table("Liste_Domain_sup_0_King.txt",header=F,quote="")
Queen<- read.table("Liste_Domain_sup_0_Queen.txt",header=F,quote="")
Soldier<-read.table("Liste_Domain_sup_0_Soldier.txt",header=F,quote="")
Worker<-read.table("Liste_Domain_sup_0_Worker.txt",header=F,quote="")


#the graphic:  
venn.diagram(
x = list(King$V1 ,Queen$V1,Soldier$V1,Worker$V1),
filename = "Venn_diagram_domains_threshold_greater_0.png",
    main="Venn diagram of differentially expressed domains \n for the castes(counts greater than 0)",
        output = TRUE ,
        imagetype="png" ,
        height = 700 , 
        width =750, 
        resolution = 200,
        compression = "lzw",
        lwd = 1,
        lty = 'blank',
    category = c("King", "Queen", "Soldier", "Worker" ),
        fill = c('#4DAC26','#5E3C99', '#018571', '#E66101'),
        cex = 1,
        fontfamily = "sans",
           cat.pos=5,
        cat.default.pos = "outer",
    cat.cex=1,
       cat.fontface = "bold"
        )




##################
##Doamins greater that 5

King <- read.table("Liste_Domain_sup_5_King.txt",header=F,quote="")
Queen<- read.table("Liste_Domain_sup_5_Queen.txt",header=F,quote="")
Soldier<-read.table("Liste_Domain_sup_5_Soldier.txt",header=F,quote="")
Worker<-read.table("Liste_Domain_sup_5_Worker.txt",header=F,quote="")


#The graphic 
venn.diagram(
x = list(King$V1 ,Queen$V1,Soldier$V1,Worker$V1),
filename = "Venn_diagram_domains_threshold_greater_5.png",
    main="Venn diagram of differentially expressed domains \n for the castes (counts greater than 5)",
        output = TRUE ,
        imagetype="png" ,
        height = 700 , 
        width =750, 
        resolution = 200,
        compression = "lzw",
        lwd = 1,
        lty = 'blank',
    category = c("King", "Queen", "Soldier", "Worker" ),
        fill = c('#4DAC26','#5E3C99', '#018571', '#E66101'),
        cex = 1,
        fontfamily = "sans",
           cat.pos=5,
        cat.default.pos = "outer",
    cat.cex=1,
       cat.fontface = "bold"
        )

##################

##Doamins greater that 10

King <- read.table("Liste_Domain_sup_10_King.txt",header=F,quote="")
Queen<- read.table("Liste_Domain_sup_10_Queen.txt",header=F,quote="")
Soldier<-read.table("Liste_Domain_sup_10_Soldier.txt",header=F,quote="")
Worker<-read.table("Liste_Domain_sup_10_Worker.txt",header=F,quote="")


#The graphic 
venn.diagram(
x = list(King$V1 ,Queen$V1,Soldier$V1,Worker$V1),
filename = "Venn_diagram_domains_threshold_greater_10.png",
    main="Venn diagram of differentially expressed domains \n for the castes (counts greater than 10)",
        output = TRUE ,
        imagetype="png" ,
        height = 700 , 
        width =750, 
        resolution = 200,
        compression = "lzw",
        lwd = 1,
        lty = 'blank',
    category = c("King", "Queen", "Soldier", "Worker" ),
        fill = c('#4DAC26','#5E3C99', '#018571', '#E66101'),
        cex = 1,
        fontfamily = "sans",
           cat.pos=5,
        cat.default.pos = "outer",
    cat.cex=1,
       cat.fontface = "bold"
        )

